var hitSlider = new Swiper('.hit-slider', {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,

    navigation: {
        nextEl: '.hit-slider-next',
        prevEl: '.hit-slider-prev',
    },

    pagination: {
        el: '.hit-slider-pagination',
        type: 'bullets',
        clickable: true,
    },
});
