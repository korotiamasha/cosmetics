var mainComponent = new Swiper('.main-component-swiper', {
    slidesPerView: 3,
    spaceBetween: 15,
    loop: true,

    navigation: {
        nextEl: '.main-component-next',
        prevEl: '.main-component-prev',
    },

    pagination: {
        el: '.main-component-pagination',
        type: 'bullets',
        clickable: true,
    },
});
