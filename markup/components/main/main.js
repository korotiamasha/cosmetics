var mainComponent = new Swiper('.main-component__slider', {
    slidesPerView: 3,
    centeredSlides: true,
    spaceBetween: 15,
    loop: true,

    navigation: {
        nextEl: '.main-component-next',
        prevEl: '.main-component-prev',
    },

    pagination: {
        el: '.main-component-pagination',
        type: 'bullets',
        clickable: true,
    },
});


var mainBlog = new Swiper('.main-blog-swiper', {
    slidesPerView: 1,
    centeredSlides: false,
    spaceBetween: 15,
    loop: true,

    navigation: {
        nextEl: '.main-blog-next',
        prevEl: '.main-blog-prev',
    },

    pagination: {
        el: '.main-blog-pagination',
        type: 'bullets',
        clickable: true,
    },
});

var mainBannerSlider = new Swiper('.main-banner__slider', {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,

    pagination: {
        el: '.main-banner-pagination',
        type: 'bullets',
        clickable: true,
    },
});

var mainProduct = new Swiper('.main-product__slider', {
    slidesPerView: 4,
    spaceBetween: 15,
    allowTouchMove: false,
    loop: true,

    navigation: {
        nextEl: '.main-product-next',
        prevEl: '.main-product-prev',
    },

    pagination: {
        el: '.main-product-pagination',
        type: 'bullets',
        clickable: true,
    },
});

var mainOffers = new Swiper('.main-offers__slider', {
    slidesPerView: 4,
    spaceBetween: 15,
    loop: true,

    navigation: {
        nextEl: '.main-offers-next',
        prevEl: '.main-offers-prev',
    },

    pagination: {
        el: '.main-offers-pagination',
        type: 'bullets',
        clickable: true,
    },
});
