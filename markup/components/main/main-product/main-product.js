$('.spinner-form .count-plus').on('click', function(){
    var calcSpinner = $(this).closest('div.spinner-form');
    var count = $(calcSpinner).find('.spinner-form__input').val();

    if ($.isNumeric(count)){
        count = parseInt(count) + 1;
        $(calcSpinner).find('.spinner-form__input').val(count);
    }
});


$('.spinner-form .count-minus').on('click', function(){
    var calcSpinner = $(this).closest('div.spinner-form');
    var count = $(calcSpinner).find('.spinner-form__input').val();

    if ($.isNumeric(count)){
        $('.calc-spinner-size').removeClass('error');
        if (parseInt(count) -1> 0) {
            count = parseInt(count) - 1;
            $(calcSpinner).find('.spinner-form__input').val(count);
        }
    }
});
