$('.check-like').on('click', function(){
    $(this).toggleClass('checked');
});

$('.check-product').on('click', function(){
    $(this).toggleClass('checked');
});

$('.p-reviews__more').on('click', function () {
    if ($(this).parent().hasClass('show')) {
        $(this).parent().removeClass('show');
        $(this).html('Развернуть все отзывы');
    } else {
        $(this).parent().addClass('show');
        $(this).html('Скрыть');
    }
});
