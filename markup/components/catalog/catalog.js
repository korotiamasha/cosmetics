$('.select2').select2({
    minimumResultsForSearch: -1,
    dropdownAutoWidth: true,
    width: 'auto',
})

$('.select2').on('select2:open', function () {
    let a = $(this).data('select2');
    a.$results.closest('.select2-dropdown').parent().addClass('select-custom');

});

$('.filter-menu .has-child').on('click', function () {
    $(this).toggleClass('active');
});
